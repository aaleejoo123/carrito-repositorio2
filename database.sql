CREATE DATABASE IF NOT EXISTS carritoshopping;
USE carritoshopping;

CREATE TABLE users(
id   int(255)  auto_increment not null,
role varchar(20),
name varchar(255),
surname varchar(255),
email varchar(255),
password varchar(255),
image varchar(255),
created_at datetime,
updated_at datetime,
remember_token varchar(255),
PRIMARY KEY(id)
)ENGINE=InnoDb;

CREATE TABLE productos(
id   int(255) auto_increment not null,
user_id int(255) not null,
title varchar(255), 
description text,
image varchar(255),
photo varchar(255),
price decimal(6,2),
created_at datetime, 
updated_at datetime,
PRIMARY KEY(id),
FOREIGN KEY(user_id) REFERENCES users(id)


)ENGINE = InnoDb;

--CREATE TABLE comments(
--id int(255) auto_increment not null,
--user_id int(255) not null,
--producto_id int(255) not null,
--body text,
--created_at datetime,
--updated_at datetime,
--CONSTRAINT pk_comment PRIMARY KEY(id),
--CONSTRAINT fk_comment_producto FOREIGN KEY(producto_id) REFERENCES productos(id),
--CONSTRAINT fk_comment_user FOREIGN KEY(user_id) REFERENCES users(id)


--)ENGINE = InnoDb;