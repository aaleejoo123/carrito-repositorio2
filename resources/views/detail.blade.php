@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Detalle de Producto') }}</div>

                <a href="{{ url('productos/') }}"  class= "btn btn-secondary btn-lg btn-block" role="button" aria-pressed="true">Productos</a>>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

{{--Detalle del producto--}} 
                <div class= "row">
                    
                        <div class="col-6">
                            <img src="{{ $productos->photo}}" width="300" height="300">
                            <h4>{{ $productos->name }}</h4>
                            <p>{{ \Illuminate\Support\Str::limit(strtolower($productos->description), 50) }}</p>
                            <p><strong>precio: </strong> {{ $productos->price }} $</p>


                        </div>
                    
                </div>
{{--Detalle del producto--}} 

                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
