@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Productos') }}</div>

                  <a href="{{ url('cart/') }}"  class= "btn btn-secondary btn-lg btn-block" role="button" aria-pressed="true">Carrito de Compras</a>>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

{{--Adicionar productos--}} 
                <div class= "row">
                    @foreach ($productos as $productos)
                        <div class="col-6">
                            <img src="{{ $productos->photo}}" width="300" height="300">
                            <h4>{{ $productos->name }}</h4>
                            <p>{{ \Illuminate\Support\Str::limit(strtolower($productos->description), 50) }}</p>
                            <p><strong>precio: </strong> {{ $productos->price }} $</p>

                            <a href="{{ url('add-to-cart/' .$productos->id) }}"  class= "btn btn-primary btn-lg  btn-block" role="button" aria-pressed="true">Agregar al carrito</a>

                            <a href="{{ url('product-detail/' .$productos->id) }}"  class= "btn btn-secondary btn-lg btn-block" role="button" aria-pressed="true">Detalle</a>>

                        </div>
                    @endforeach
                </div>
{{--Adicionar productos--}} 

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
