<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\productos;

class productosController extends Controller
{
    public function productos()

   {
   		$productos = productos::all();
   		return view('productos', compact('productos'));
   }
    public function detail($id)
    {
    	$product = Productos::find($id);
    	return view('detail')->with('productos', $product);
    }

     public function cart()
    {
    	
    	return view('cart');
    }

    public function addToCart($id)
    {
       // return view('cart');

    	//Aqui esta la logica en donde se agregan los productos 
        $product = Productos::find($id);
    	$cart = session()->get('cart');
    
    	//Si el carro esta vacio entonces se add el primer producto 

    	if(!$cart) {
               
    		$cart = [
    				$id =>[
    					"name"=> $product->name,
    					"quantity"=> 1,
    					"price"=> $product->price,
    					"photo"=> $product->photo
    				]
    		];
    		session()->put('cart', $cart);

    		return redirect()->back()->with('success','product added to cart successfully!');
    	}

    	//Si el carro no esta lleno entonces se revisa si el producto existe, entonces incrementa la cantidad 

    	if(isset($cart[$id])) {

    		$cart[$id]['quantity']++;

    		session()->put('cart', $cart);

    		return redirect()->back()->with('success','product added to cart successfully!');

    	}

    	//si el producto no existe en el carro entonces se agrega a el carro qith quantity = 1 

    	$cart[$id]=[
    				"name"=> $product->name,
    				"quantity"=> 1,
    				"price"=> $product->price,
    				"photo"=> $product->photo
    	];
    	    session()->put('cart', $cart);

    		return redirect()->back()->with('success','product added to cart successfully!');

    }
}
