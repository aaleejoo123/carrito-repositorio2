<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class productos extends Model
{
	protected $table ='productos';
	// relation One to many
/*	public function comments(){
		return $this->hasMany('App\Comment'); 
	}*/
    //relation many to one
	public function user(){
		return $this ->belongsTo('App\User','user_id');
	}
    use HasFactory;
}
