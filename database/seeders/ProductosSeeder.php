<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('productos')->insert([
            'name' => 'Hamburguesa',
            'description' => 'Hamburguesa con doble queso',
            'photo' => 'https://cocina-casera.com/wp-content/uploads/2016/11/hamburguesa-queso-receta.jpg',
            'price' => '12',
             ]);

        DB::table('productos')->insert([
            'name' => 'Tacos',
            'description' => 'tacos al pastor',
            'photo' => 'https://img.vixdata.io/pd/webp-large/es/sites/default/files/imj/elgranchef/C/Condimento%20para%20tacos%20hecho%20en%20casa%202.jpg',
            'price' => '15',
             ]);

        DB::table('productos')->insert([
            'name' => 'Perro Caliente',
            'description' => 'Perro caliente con salchicha americana',
            'photo' => 'https://cocina-casera.com/wp-content/uploads/2011/10/perrito-caliente.jpg',
            'price' =>'10',
             ]);

        DB::table('productos')->insert([
            'name' => 'Empanada',
            'description' => 'Empanada de pollo o carne',
            'photo' => 'https://www.vanguardia.com/binrepository/717x477/1c0/716d477/none/12204/BOSB/web_empanada_big_tp_VL444624_MG18506670.jpg',
            'price' => '6',
             ]);

    }
}
